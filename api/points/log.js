import request from '@/utils/request'

// api地址
const api = {
  list: '/user/integral.html'
}

// 积分明细列表
export const list = (param) => {
  return request.get(api.list, param)
}
