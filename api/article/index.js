import request from '@/utils/request'

// api地址
const api = {
}

// 文章列表
export function list(param, option) {
	let api = '/article/list.html'
	if (param.catId) {
		api = '/article/list/' + param.catId + '.html'
	}
	return request.get(api, param, option)
}

// 文章详情
export function detail(articleId) {
	return request.get('/article/' + articleId + '.html')
}
